﻿/****************************************************************************************/
/*                       Projecto : Roteiros Turisticos                                 */
/*                                                                                      */
/*                       Autores                                                        */
/*                       Vitor Oliveira : 50031133                                      */
/*                       Inês Pires : 50030921                                          */
/*                       Taslima Abedin : 59931067                                      */
/*                       Lidiane Miranda : 50032576                                     */
/*                       André Teodoro : 50032748                                       */
/*                                                                                      */
/*                       Universidade Europeia 2014                                     */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*							         JAVASCRIPT MAIN                                    */
/****************************************************************************************/

$(document).ready(function(){
    $(window).resize(function(){
        var w = $(window).width(),z=1;
        if(w<800) w=800;
        z= w/1500;
        $('body').css('zoom',z);
    });
    $(window).trigger('resize');
    $('#slideshow').cycle({
        fx:     'scrollLeft',
        easing: 'easeOutBack',
        timeout:4000,
        speed:3000,
        next:'#slideSeguinte',
        prev:'#slideAnterior',
        pager:'#slidePager',
        pagerAnchorBuilder: function(index, el) {
            return '<a href="#"></a>';
        }
    });
});


$(document).ready(function(){
    $("#newletter [name='OK']").click(function(){
        $("#newletter").submit();
    });
});

var fullUrl=function( relativeUrl ) {
    return $('<a href="'+relativeUrl+'"></a>')[0].href.toString();
}

$(document).ready(function(){
    $("#painel_rotas .rota").append('<a href="detalhe.html" style="display:none">abre detalhe</a>')
        .click(function(e){
            window.document.location.assign( fullUrl('detalhe.html') );
        });
});

$(document).ready(function(){
    if($.validator)
        $.extend($.validator.messages, {
            required: "Este campo é obrigatório.",
            remote: "Por favor corrija este campo.",
            email: "Por favor insira um endereço de e-mail válido.",
            url: "Por favor digite uma URL válida.",
            date: "Por favor, insira uma data válida.",
            dateISO: "Por favor, insira uma data válida (ISO).",
            number: "Por favor insira um número válido.",
            digits: "Por favor, indique apenas dígitos.",
            creditcard: "Por favor, insira um número de cartão de crédito válido.",
            equalTo: "Por favor, insira o mesmo valor novamente.",
            accept: "Por favor preencha com uma extensão válida.",
            maxlength: $.validator.format("Digite não mais do que {0} caracteres."),
            minlength: $.validator.format("Por favor, insira pelo menos {0} caracteres."),
            rangelength: $.validator.format("Por favor insira um valor entre {0} e {1} caracteres."),
            range: $.validator.format("Por favor, insira um valor entre {0} e {1}."),
            max: $.validator.format("Por favor insira um valor inferior ou igual a {0}."),
            min: $.validator.format("Por favor insira um valor maior ou igual a {0}.")
        });
});
